from urllib.parse import urlparse, parse_qs

from environs import Env

env = Env()
env.read_env()


def get_database_config(ssl_require=False, conn_max_age: int = 600) -> dict[str]:
    """
    Parses a PostgreSQL database URL
    If DATABASE_URL is not present in the environment, throws an error

    Format:  postgres://<username>:<password>@<host>[:port]/<name>
    """

    url = urlparse(env.str('DATABASE_URL'))
    params = parse_qs(url.query)

    options = {}
    for k, v in params.items():
        options[k.upper()] = v[0]

    if ssl_require:
        options['sslmode'] = 'require'

    return {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': url.path[1:],
        'HOST': url.hostname,
        'PORT': url.port or 5432,
        'USER': url.username,
        'PASSWORD': url.password,
        'CONN_MAX_AGE': conn_max_age,
        'OPTIONS': options,
    }


def get_heroku_hosts() -> list[str]:
    """Constructs a singleton list that contains the hostname, using the HEROKU_APP_NAME environment variable"""
    app_name = env.str('HEROKU_APP_NAME')
    return [f'{app_name}.herokuapp.com']
