from django.conf import settings
from hashids import Hashids

hashid_len = 8
hasher = Hashids(salt=settings.HASHID_SALT, min_length=hashid_len)


def h_encode(pk):
    return hasher.encode(pk)


def h_decode(h):
    return hasher.decode(h)


class HashIdConverter:
    regex = rf'[a-zA-Z0-9]{{{hashid_len},}}'

    @staticmethod
    def to_python(value):
        ret = h_decode(value)
        return ret[0] if len(ret) == 1 else ret

    @staticmethod
    def to_url(value):
        return h_encode(value)
