# ApiEval Web

---

This repository contains a web app for the [apievaluator](https://pypi.org/project/apievaluator/) Python library.  
Built with Django and React.

## Documentation

The final paper (in Croatian) is available at https://www.overleaf.com/read/ytbqjjqjgsyf

## Deployment

| Environment | URL                                  |
|:------------|:-------------------------------------|
| Staging     | http://apieval-staging.herokuapp.com |
| Production  | http://apieval.herokuapp.com         |

## Local development

### Requirements

You will need:

- Python: https://www.python.org/
- Poetry: https://python-poetry.org/
- Node.js: https://nodejs.org/
- pnpm: https://pnpm.io/
- docker: https://www.docker.com/
- docker-compose: https://docs.docker.com/compose/install/

### Environment variables

You can provide the required environment variables in a `.env` file in the root of the project.  
Note that `DEBUG` is optional, and defaults to `False`.

```dotenv
DEBUG=True
SECRET_KEY=a-random-key
HASHID_SALT=some-other-random-key
DATABASE_URL=postgres://<username>:<password>@<host>[:port]/<name>
```

The recommended method to generate secret keys is with django utils:

```shell
$ python -c 'from django.core.management import utils; print(utils.get_random_secret_key())'
```

**IMPORTANT: Make sure to generate a separate key for `HASHID_SALT`**

### 2. Running the app

1. Install dependencies

   ```shell
   $ poetry install
   $ npm install
   ```

2. Start the database

  ```shell
  $ docker-compose up
  ```

3. Run database migrations

   ```shell
   $ poetry run python manage.py migrate
   ```

4. Start the frontend development server

   ```shell
   $ npm run dev
   ```

5. Run the python server in another shell

   ```shell
   $ poetry run python manage.py runserver
   ```

6. The website should be available at `http://localhost:8000`

## Project structure

```text
apieval-web
├── apieval_web   - Django project settings
├── backend       - API app
├── frontend      - Frontend app
│   └── src       - Frontend source code
└── users         - Authorization app
```

## License

[MIT](./LICENSE)
