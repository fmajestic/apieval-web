import logging
import traceback

from apievaluator.logic import eval_all
from background_task import background
from django.utils import timezone
from yaml import load, FullLoader

from backend.models import TestResult


@background(schedule=0)
def run_test(spec, content, url, result_id):
    log = logging.getLogger(f'{__name__}.run_test')

    try:
        log.info(f'Calling apievaluator for test {result_id}')

        (exists, responses, summary) = eval_all(paths=spec['paths'], tests=load(content, Loader=FullLoader), apiurl=url)
        result_dic = {
            'exists': exists,
            'responses': responses,
            'summary': summary
        }

        log.info(f'Apievaluator call finished for test {result_id}')

        result = TestResult.objects.get(pk=result_id)
        result.completed_at = timezone.now()
        result.results = result_dic
        result.success = summary.get('failed') == 0
        result.save()
    except Exception as ex:
        log.warning(f'Apievaluator failed with: {repr(ex)}')
        traceback.print_exc()
        result = TestResult.objects.get(pk=result_id)
        result.completed_at = timezone.now()
        result.results = {'failed': str(ex)}
        result.success = False
        result.save()
