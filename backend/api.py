import logging
import re
from typing import cast

import yaml
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.utils import timezone
# noinspection PyProtectedMember
from prance import BaseParser, ResolvingParser
from rest_framework import viewsets, views, status
from rest_framework.decorators import action
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.request import Request
from rest_framework.response import Response

from users.models import User
from .models import TestSuite, TestResult
from .serializers import *
from .tasks import run_test


class TestSuiteViewSet(viewsets.ModelViewSet):
    log = logging.getLogger(f'{__name__}.{__qualname__}')

    serializer_class = TestSuiteSerializer

    def get_queryset(self):
        return cast(User, self.request.user).tests.all()

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user)


class TestSuiteDetailViewSet(viewsets.ModelViewSet):
    log = logging.getLogger(f'{__name__}.{__qualname__}')

    serializer_class = TestSuiteDetailSerializer

    def get_queryset(self):
        return cast(User, self.request.user).tests.all()

    @action(methods=['post'], detail=True)
    def run(self, *args, **kwargs):
        instance: TestSuite = self.get_object()

        result = TestResult(started_at=(timezone.now()), )
        instance.results.add(result, bulk=False)
        instance.save()

        self.log.info(f'Starting test {result.id} ({instance.url}) in background job')
        run_test(
            spec=BaseParser(spec_string=instance.spec).specification,
            content=instance.content,
            url=instance.url,
            result_id=result.id
        )

        return Response(TestResultSerializer(result).data)


class SpecUploadView(views.APIView):
    log = logging.getLogger(f'{__name__}.{__qualname__}')

    parser_classes = (MultiPartParser, FormParser)

    def post(self, request: Request):
        spec: InMemoryUploadedFile = request.data['spec']
        (name, _, ext) = spec.name.rpartition('.')

        if ext not in ['json', 'yaml']:
            return Response({'msg': {'fileType': 'Only accepts JSON or YAML file'}},
                            status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        spec_string = spec.file.getvalue().decode(request.data.encoding)
        parsed = ResolvingParser(spec_string=spec_string)

        title = parsed.specification.get('info', {}).get('title')
        url = parsed.specification.get('servers', [])[0].get('url')
        spec_type = TestSuite.SpecificationType[ext.upper()]

        test_defs = dict()

        for path, methods in parsed.specification.get('paths', dict()).items():
            path_def = dict()
            path_def['methods'] = [key.upper() for key in methods.keys()]

            for method, conf in methods.items():
                method_def = dict()

                for param in conf.get('parameters', []):
                    # Currently, supporting only 1 path parameter
                    if param.get('in') == 'path':
                        method_def['req'] = {
                            'in': 'both' if re.search(r'\{.+\}', path) else 'path',
                            'Id': param.get('schema', {}).get('example')
                        }
                        break

                request_props = conf.get('requestBody', {}).get('content', {}).get('application/json', {})

                if request_props.get('type') == 'object':
                    method_def['req'] = {
                        'in': 'body',
                        'type': 'object',
                        'object': {}
                    }

                    for prop, types in request_props.get('properties', {}):
                        method_def['req']['object'][prop] = types.get('example', '')

                response_props = conf.get('responses', {}).get('200', {}).get('application/json', {}).get('schema', {})

                if response_props.get('type') == 'object':
                    method_def['resp'] = {
                        'in': 'body',
                        'type': 'object',
                        'object': {}
                    }

                    for prop, propConf in response_props.get('properties', {}).items():
                        the_type = propConf.get('type')
                        method_def['resp']['object'][prop] = the_type[:3] if the_type == 'integer' else the_type

                elif response_props.get('type') == 'array':
                    method_def['resp'] = {
                        'in': 'body',
                        'type': 'array'
                    }

                if method_def:
                    path_def[method.upper()] = method_def

            if path_def:
                test_defs[path] = path_def

        content = yaml.dump({'endpoints': test_defs})

        test_suite = request.user.tests.create(title=title, url=url, spec=spec_string, spec_type=spec_type,
                                               content=content)

        self.log.info(f'File upload successful')

        return Response(TestSuiteSerializer(test_suite).data)
