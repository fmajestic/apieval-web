from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from users.models import User
from utils.hashing import h_encode


class TestSuite(models.Model):
    class SpecificationType(models.TextChoices):
        JSON = 'JSON', 'JSON'
        YAML = 'YAML', 'YAML'

    title = models.CharField('title for the tests', max_length=128)
    url = models.URLField('URL of the api to test', max_length=255)
    spec = models.TextField('OpenAPI specification', null=True)
    spec_type = models.CharField('Type of OpenAPI specification', max_length=4, null=True,
                                 choices=SpecificationType.choices)
    content = models.TextField('YAML test definitions', null=True)
    created_at = models.DateTimeField('Date created', auto_now_add=True)

    user = models.ForeignKey(User, related_name='tests', on_delete=models.DO_NOTHING, null=True)

    class Meta:
        db_table = 'test_suite'

    def last_test(self):
        try:
            return self.results.latest('started_at')
        except ObjectDoesNotExist:
            return None

    def get_hashid(self):
        return h_encode(self.id)


class TestResult(models.Model):
    started_at = models.DateTimeField('Start time')
    completed_at = models.DateTimeField('End time', null=True)
    success = models.BooleanField('Did all tests pass', null=True, default=None)

    # TODO: make a table?
    #   (endpoint, request(method, headers, data), response(status, headers, data), success, message (from the library))
    results = models.JSONField('Test results', default=dict)

    test_suite = models.ForeignKey(TestSuite, on_delete=models.CASCADE, related_name='results')

    class Meta:
        db_table = 'test_result'
        ordering = ['-started_at']

    def get_hashid(self):
        return h_encode(self.id)
