from django.urls import path, register_converter

from utils.hashing import HashIdConverter
from .api import TestSuiteViewSet, TestSuiteDetailViewSet, SpecUploadView

register_converter(HashIdConverter, 'hashid')

test_list = TestSuiteViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

test_detail = TestSuiteDetailViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

test_run = TestSuiteDetailViewSet.as_view({'post': 'run'})

spec_upload = SpecUploadView.as_view()

urlpatterns = [
    path('api/tests', test_list, name='test-list'),
    path('api/tests/spec', spec_upload, name='spec-upload'),
    path('api/tests/<hashid:pk>', test_detail, name='test-detail'),
    path('api/tests/<hashid:pk>/run', test_run, name='test-run'),
]
