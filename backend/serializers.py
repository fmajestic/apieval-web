from rest_framework import serializers

from .models import TestSuite, TestResult


def hashid_field():
    return serializers.CharField(source='get_hashid', read_only=True)


class TestResultSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestResult
        fields = ('started_at', 'completed_at', 'success')


class TestResultSerializer(serializers.ModelSerializer):
    id = hashid_field()

    class Meta:
        model = TestResult
        exclude = ('test_suite',)


class TestSuiteSerializer(serializers.ModelSerializer):
    id = hashid_field()
    last_test = TestResultSimpleSerializer(read_only=True)

    class Meta:
        model = TestSuite
        exclude = ('user',)
        extra_kwargs = {
            'url': {'write_only': True},
            'spec': {'write_only': True},
            'spec_type': {'write_only': True},
            'content': {'write_only': True},
        }


class TestSuiteDetailSerializer(serializers.ModelSerializer):
    id = hashid_field()
    results = TestResultSerializer(many=True, read_only=True)

    class Meta:
        model = TestSuite
        exclude = ('user',)


__all__ = ['TestResultSerializer', 'TestResultSimpleSerializer', 'TestSuiteDetailSerializer', 'TestSuiteSerializer']
