import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { loadTestDetails } from '../../actions/tests';
import TestForm from './TestForm';

const TestEdit = ({ match, init, loadTestDetails }) => {
  const id = match.params.id;

  useEffect(() => {
    loadTestDetails(id);
  }, [id, init, loadTestDetails]);

  return <TestForm id={id} init={init} />;
};

TestEdit.propTypes = {
  match: PropTypes.object.isRequired,
  init: PropTypes.object,
  loadTestDetails: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  init: ownProps.id ? state.tests.testDetails[ownProps.id] : null,
});

export default connect(mapStateToProps, { loadTestDetails })(TestEdit);
