import React, { useEffect } from 'react';
import { Button, Table } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  faCheckCircle,
  faSpinner,
  faTimesCircle,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

import { deleteTest, getTests } from '../../actions/tests';

const TestList = ({ tests, getTests }) => {
  useEffect(() => {
    getTests();
  }, [getTests]);

  const mapLastToText = (last) => {
    if (last === null) return 'Not ran yet';
    if (last.completedAt)
      return `Finished at ${new Date(last.completedAt).toLocaleString()}`;
    if (last.startedAt)
      return `Started at ${new Date(last.startedAt).toLocaleString()}`;
  };

  const mapLastToIcon = (last) => {
    if (last === null) return '-';
    if (last.completedAt)
      return last.success ? (
        <FontAwesomeIcon icon={faCheckCircle} color="green" />
      ) : (
        <FontAwesomeIcon icon={faTimesCircle} color="red" />
      );
    if (last.startedAt) return <FontAwesomeIcon icon={faSpinner} spin />;
  };

  return (
    <Table striped>
      <thead>
        <tr>
          <th>Title</th>
          <th>Last run</th>
          <th>Result</th>
          <th className="text-end">Actions</th>
        </tr>
      </thead>
      <tbody className="align-middle">
        {tests.map((test) => {
          return (
            <tr key={test.id}>
              <td>{test.title}</td>
              <td>{mapLastToText(test.lastTest)}</td>
              <td>{mapLastToIcon(test.lastTest)}</td>
              <td className="text-end">
                <Button as={Link} to={`/tests/${test.id}`} variant="info">
                  Open
                </Button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

TestList.propTypes = {
  tests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      lastTest: PropTypes.shape({
        startedAt: PropTypes.string.isRequired,
        completedAt: PropTypes.string,
        success: PropTypes.bool.isRequired,
      }),
    })
  ).isRequired,
  getTests: PropTypes.func.isRequired,
  deleteTest: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  tests: state.tests.tests,
});

export default connect(mapStateToProps, { getTests, deleteTest })(TestList);
