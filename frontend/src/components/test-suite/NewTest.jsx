import React, { useState } from 'react';
import { Button, Col, Form, Row, Tab, Tabs } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { createFromFile } from '../../actions/tests';
import TestForm from './TestForm';

const NewTest = ({ initialTab = 'file', createFromFile }) => {
  const [file, setFile] = useState(null);
  const [testId, setTestId] = useState(null);

  const onChange = (e) => {
    setFile(e.target.files[0]);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    createFromFile(file).then((id) => {
      setTestId(id);
    });
  };

  if (testId) {
    return <Redirect push to={`/tests/${testId}`} />;
  }

  return (
    <Tabs defaultActiveKey={initialTab} className="mb-3">
      <Tab eventKey="file" title="From a file">
        <Form onSubmit={onSubmit}>
          <Row>
            <Col>
              <Form.Group controlId="spec-file">
                <Form.Label>OpenAPI specification</Form.Label>
                <Form.Control
                  type="file"
                  name="spec"
                  required
                  accept=".json,.yaml"
                  onChange={onChange}
                />
              </Form.Group>
            </Col>
            <Col className="align-self-end">
              <Button variant="primary" type="submit">
                Create test
              </Button>
            </Col>
          </Row>
        </Form>
        <br />
        <h5>Note:</h5>
        <p>
          Automatic test generation is still limited. You are advised to check
          the generated tests and make adjustments if necessary.
        </p>
      </Tab>
      <Tab eventKey="custom" title="Custom definition">
        <TestForm />
      </Tab>
    </Tabs>
  );
};

NewTest.propTypes = {
  initialTab: PropTypes.string,
  createFromFile: PropTypes.func.isRequired,
};

export default connect(null, { createFromFile })(NewTest);
