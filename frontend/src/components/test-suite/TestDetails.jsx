import React, { useEffect, useState } from 'react';
import { Button, Modal, Table } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  faCheckCircle,
  faSpinner,
  faTimesCircle,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

import { deleteTest, loadTestDetails, runTest } from '../../actions/tests';

const TestDetails = ({ test, loadTestDetails, runTest, deleteTest, match }) => {
  const id = match.params.id;
  const [showModal, setShowModal] = useState(false);
  const [modalText, setModalText] = useState('');

  useEffect(() => {
    loadTestDetails(id);
  }, [id, loadTestDetails]);

  const dateFmt = (isoString) =>
    isoString ? new Date(isoString).toLocaleString() : '-';

  const onRun = () => {
    runTest(id);
  };

  if (!test) {
    return <h2>Loading...</h2>;
  }

  return (
    <>
      <h3>Results: {test.title}</h3>
      <div className="float-end">
        <Button variant="success" onClick={onRun}>
          Run test
        </Button>
        <Button
          variant="primary"
          className="ms-3"
          onClick={() => loadTestDetails(id)}
        >
          Refresh
        </Button>
        <Button as={Link} to={`/edit/${id}`} className="ms-3" variant="light">
          Edit
        </Button>
      </div>
      <p>{test.url}</p>
      <Table striped>
        <thead>
          <tr>
            <th>ID</th>
            <th>Started at</th>
            <th>Completed at</th>
            <th>Status</th>
            <th className="text-end">Actions</th>
          </tr>
        </thead>
        <tbody className="align-middle">
          {test.results.map((result) => (
            <tr key={result.id}>
              <td>{result.id}</td>
              <td>{dateFmt(result.startedAt)}</td>
              <td>{dateFmt(result.completedAt)}</td>
              <td>
                {result.success ? (
                  <FontAwesomeIcon icon={faCheckCircle} color="green" />
                ) : result.success === false ? (
                  <FontAwesomeIcon icon={faTimesCircle} color="red" />
                ) : (
                  <FontAwesomeIcon icon={faSpinner} spin />
                )}
              </td>
              <td className="text-end">
                <Button
                  variant="info"
                  onClick={() => {
                    setModalText(JSON.stringify(result.results));
                    setShowModal(true);
                  }}
                >
                  View output
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <Modal
        size="lg"
        show={showModal}
        onHide={() => setShowModal(false)}
        aria-labelledby="modal-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="modal-title">Test result output</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <pre>
            {modalText.length > 0
              ? JSON.stringify(JSON.parse(modalText), null, 2)
              : ''}
          </pre>
        </Modal.Body>
      </Modal>
    </>
  );
};

TestDetails.propTypes = {
  test: PropTypes.shape({
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    results: PropTypes.arrayOf(
      PropTypes.shape({
        startedAt: PropTypes.string.isRequired,
        completedAt: PropTypes.string.isRequired,
        success: PropTypes.bool.isRequired,
      })
    ).isRequired,
  }),
  runTest: PropTypes.func.isRequired,
  loadTestDetails: PropTypes.func.isRequired,
  deleteTest: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  test: state.tests.testDetails[ownProps.match.params.id],
});

export default connect(mapStateToProps, {
  loadTestDetails,
  runTest,
  deleteTest,
})(TestDetails);
