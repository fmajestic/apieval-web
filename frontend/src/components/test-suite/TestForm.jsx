import React, { useState } from 'react';
import { Button, Col, Form, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Editor from '@monaco-editor/react';
import PropTypes from 'prop-types';

import { createTest, updateTest } from '../../actions/tests';

const TestForm = ({ id = null, init, createTest, updateTest }) => {
  const initialData = init || {
    title: '',
    url: '',
    content: '',
    spec: null,
  };

  console.log(init);

  const [data, setData] = useState(initialData);
  const [formSubmitted, setFormSubmitted] = useState(false);

  const onChange = (event) => {
    setData({
      ...data,
      [event.target.name]: event.target.files
        ? event.target.files[0]
        : event.target.value,
    });
  };

  const onSubmit = (event) => {
    event.preventDefault();
    if (id) {
      updateTest(id, data.title, data.content, data.url, data.spec).then(() =>
        setFormSubmitted(true)
      );
    } else {
      createTest(data.title, data.content, data.url, data.spec).then(() =>
        setFormSubmitted(true)
      );
    }
  };

  if (formSubmitted) {
    return <Redirect push to={`/tests/${id}`} />;
  }

  return (
    <Form onSubmit={onSubmit}>
      <Row className="gx-5">
        <Col sm={4}>
          {/* TODO: change this to 'name' (don't forget the model, too!) */}
          <Form.Group as={Row} className="mb-3" controlId="title-field">
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              name="title"
              value={data.title}
              onChange={onChange}
              placeholder="Pet store API"
              required
            />
          </Form.Group>

          <Form.Group as={Row} className="mb-3" controlId="url-field">
            <Form.Label>URL</Form.Label>
            <Form.Control
              type="text"
              name="url"
              value={data.url}
              onChange={onChange}
              placeholder="https://example.com/api/v1"
              required
            />
          </Form.Group>

          <Form.Group as={Row} className="mb-3" controlId="spec-file">
            <Form.Label>OpenAPI specification</Form.Label>
            <Form.Control
              type="file"
              name="spec"
              accept=".json,.yaml"
              required
              onChange={onChange}
            />
          </Form.Group>

          <Row>
            <p>
              Note:
              <br />
              Automatic test generation is supported only when uploading a file
              directly
            </p>
          </Row>
        </Col>
        <Col>
          <Row className="text-end">
            <a
              href="https://gitlab.com/fmajestic/api-evaluator"
              target="_blank"
              rel="noreferrer"
            >
              For test syntax, see the documentation of Api Evaluator
            </a>
          </Row>
          <Row>
            <p className="mb-2">Tests</p>
            {/* TODO: consider switching to react-simple-code-editor */}
            <Editor
              height="40rem"
              theme="vs-dark"
              defaultLanguage="yaml"
              options={{
                tabSize: 2,
                fontSize: 17,
                lineNumbersMinChars: 2,
                wordWrap: 'off',
                minimap: { enabled: false },
                renderFinalNewline: true,
                emptySelectionClipboard: false,
              }}
              className="border border-light rounded-3"
              onChange={(content) => setData({ ...data, content })}
            />
          </Row>
        </Col>
      </Row>
      <Button variant="primary" type="submit" className="mt-3">
        {id ? 'Save' : 'Create'}
      </Button>
    </Form>
  );
};

TestForm.propTypes = {
  id: PropTypes.string,
  init: PropTypes.object,
  createTest: PropTypes.func.isRequired,
  updateTest: PropTypes.func.isRequired,
};

export default connect(null, { createTest, updateTest })(TestForm);
