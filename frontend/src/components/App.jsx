import React, { useEffect } from 'react';
import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

import { loadUser } from '../actions/auth';
import store from '../store';
import Page from './common/Page';
import Alerts from './layout/Alerts';
import Dashboard from './layout/Dashboard';
import Navigation from './layout/Navigation';
import NewTest from './test-suite/NewTest';
import TestDetails from './test-suite/TestDetails';
import TestEdit from './test-suite/TestEdit';
import Account from './users/Account';
import Login from './users/Login';
import Register from './users/Register';

import styles from './App.scss';

const alertSettings = {
  timeout: 10000,
  transition: 'scale',
  position: 'top center',
  offset: '.5rem',
  containerStyle: {
    zIndex: styles.alertZIndex,
    // TODO: remove the uppercase text?
  },
};

/*
 TODO:
  - theme switcher (on off canvas?)
    https://react-bootstrap.netlify.app/components/offcanvas
  - replace react-alerts with "native" toasts
    https://react-bootstrap.netlify.app/components/toasts
*/
export default function App() {
  useEffect(() => {
    store.dispatch(loadUser());
  }, []);

  return (
    <Provider store={store}>
      <AlertProvider template={AlertTemplate} {...alertSettings}>
        <Router>
          <Navigation />
          <Alerts />
          <Switch>
            <Page exact path="/" title="Dashboard" component={Dashboard} />
            <Page exact path="/new" title="New test" component={NewTest} />
            <Page path="/tests/:id" title="Details" component={TestDetails} />
            <Page path="/edit/:id" title="Details" component={TestEdit} />
            <Page exact path="/account" title="Account" component={Account} />
            <Page
              exact
              isPublic
              path="/login"
              title="Login"
              component={Login}
            />
            <Page
              exact
              isPublic
              path="/register"
              title="Register"
              component={Register}
            />
          </Switch>
        </Router>
      </AlertProvider>
    </Provider>
  );
}
