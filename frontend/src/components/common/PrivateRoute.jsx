import React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import { oneOf } from '../../util/props';

const PrivateRoute = ({
  component: Component,
  render,
  isLoading,
  isAuthenticated,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      // TODO: move this out of render() and return 3 separate components?
      render={(props) => {
        if (isLoading === true) {
          return <h2>Loading...</h2>; // TODO: add spinner or something
        } else if (isAuthenticated === false) {
          return <Redirect to="/login" />;
        } else {
          if (Component) {
            return <Component {...props} />;
          } else if (render) {
            return render(props);
          } else {
            // This should never happen
            console.error(
              'PrivateRoute error: no component and no render function'
            );
            return null;
          }
        }
      }}
    />
  );
};

PrivateRoute.propTypes = {
  component: PropTypes.elementType,
  render: PropTypes.func,
  isLoading: PropTypes.bool.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  _: oneOf('component', 'render'),
};

const mapStateToProps = (state) => ({
  isLoading: state.auth.isLoading,
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps)(PrivateRoute);
