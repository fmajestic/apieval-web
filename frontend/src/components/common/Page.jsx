import React, { useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import PrivateRoute from './PrivateRoute';

export default function Page({ title, isPublic = false, ...rest }) {
  useEffect(() => {
    document.title = `${title} | ApiEval`;
  }, [title]);

  const TheRoute = isPublic ? Route : PrivateRoute;

  return (
    <Container>
      <TheRoute {...rest} />
    </Container>
  );
}

Page.propTypes = {
  title: PropTypes.string.isRequired,
  isPublic: PropTypes.bool,
};
