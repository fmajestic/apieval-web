import React from 'react';
import { Button, Container, Nav, Navbar } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import { logout } from '../../actions/auth';

// TODO:
//   check nav link highlighting
//   https://github.com/react-bootstrap/react-bootstrap/issues/5750
const Navigation = ({ username, isAuthenticated, logout }) => {
  const guestHeader = (
    <>
      <Button as={Link} to="/login" variant="primary" className="me-1">
        Log in
      </Button>
      <Button as={Link} to="/register" variant="secondary">
        Register
      </Button>
    </>
  );

  const userHeader = (
    <>
      <Navbar.Text className="me-2">Welcome, {username}</Navbar.Text>
      <Button variant="secondary" onClick={logout}>
        Log out
      </Button>
    </>
  );

  return (
    <Navbar fixed="top" variant="dark" bg="primary">
      <Container fluid>
        <Navbar.Brand as="h1" className="mb-1">
          <Link to="/" className="text-reset text-decoration-none">
            ApiEval
          </Link>
        </Navbar.Brand>
        <Nav className="me-auto">
          <NavLink exact to="/" className="nav-link" activeClassName="active">
            Dashboard
          </NavLink>
          <NavLink to="/account" className="nav-link" activeClassName="active">
            Account
          </NavLink>
        </Nav>
        {isAuthenticated ? userHeader : guestHeader}
      </Container>
    </Navbar>
  );
};

Navigation.propTypes = {
  logout: PropTypes.func.isRequired,
  username: PropTypes.string,
  isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  username: state.auth.user?.username,
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { logout })(Navigation);
