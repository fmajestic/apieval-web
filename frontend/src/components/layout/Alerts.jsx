import React, { Fragment, useEffect } from 'react';
import { useAlert } from 'react-alert';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const Alerts = ({ errors, messages }) => {
  const alert = useAlert();

  const capitalize = (str) => {
    return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
  };

  useEffect(() => {
    const { msg } = errors;

    for (let name of Object.keys(msg)) {
      const text = msg[name];
      const displayText = Array.isArray(text) ? text.join() : text;

      if (['username', 'email', 'password'].includes(name)) {
        alert.error(`${capitalize(name)}: ${displayText}`);
      } else if (['nonFieldErrors'].includes(name)) {
        alert.error(displayText);
      }
    }
  }, [alert, errors]);

  useEffect(() => {
    for (let name of Object.keys(messages)) {
      if (name === 'passwordMismatch') {
        alert.error(messages[name]);
      } else {
        alert.success(messages[name]);
      }
    }
  }, [alert, messages]);

  return <Fragment />;
};

Alerts.propTypes = {
  errors: PropTypes.shape({
    msg: PropTypes.object,
    status: PropTypes.number,
  }),
  messages: PropTypes.object,
};

const mapStateToProps = (state) => ({
  errors: state.errors,
  messages: state.messages,
});

export default connect(mapStateToProps)(Alerts);
