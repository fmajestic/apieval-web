import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import TestList from '../test-suite/TestList';

export default function Dashboard() {
  return (
    <>
      <h3 className="d-inline">Existing tests</h3>
      <Button as={Link} to="/new" variant="info" className="float-end">
        Add a new test suite <FontAwesomeIcon icon={faPlusCircle} />
      </Button>
      <hr />
      <TestList />
    </>
  );
}
