import React, { useState } from 'react';
import { Button, Col, Form, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';

const SingleFieldForm = ({
  name,
  label,
  onSubmit,
  initialValue = '',
  buttonText = 'Change',
  ...rest
}) => {
  const [data, setData] = useState(initialValue);

  return (
    <Form
      onSubmit={(event) => {
        event.preventDefault();
        onSubmit(data);
      }}
    >
      <Form.Group as={Row} controlId={`${name}-input`}>
        <Form.Label column sm={3}>
          {label}
        </Form.Label>
        <Col sm={9}>
          <div className="input-group">
            <Form.Control
              {...rest}
              name={name}
              value={data}
              onChange={({ target: { value } }) => setData(value)}
              required
            />
            <Button variant="primary" type="submit">
              {buttonText}
            </Button>
          </div>
        </Col>
      </Form.Group>
    </Form>
  );
};

SingleFieldForm.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  initialValue: PropTypes.any,
  buttonText: PropTypes.string,
};

export default SingleFieldForm;
