import React, { useState } from 'react';
import { Button, Col, Form, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { changePassword } from '../../actions/auth';

const PasswordChange = ({ username, changePassword }) => {
  const initialState = {
    currentPassword: '',
    newPassword: '',
    newPassword2: '',
  };

  const [data, setData] = useState(initialState);

  const onChange = (e) => setData({ ...data, [e.target.name]: e.target.value });

  // TODO: use react-hook-form
  //  https://react-hook-form.com/get-started

  return (
    <Form
      onSubmit={(e) => {
        e.preventDefault();
        changePassword(data.currentPassword, data.newPassword).then(() =>
          setData(initialState)
        );
      }}
    >
      <Form.Control
        readOnly
        type="text"
        name="username"
        value={username}
        onChange={onChange}
        autoComplete="username"
        style={{ display: 'none' }}
      />

      <Form.Group as={Row} controlId="old-password-input">
        <Form.Label column sm={4}>
          Old password
        </Form.Label>
        <Col sm={8}>
          <Form.Control
            type="password"
            name="currentPassword"
            autoComplete="current-password"
            value={data.currentPassword}
            onChange={onChange}
            required
          />
        </Col>
      </Form.Group>

      <Form.Group as={Row} controlId="new-password-input">
        <Form.Label column sm={4}>
          New password
        </Form.Label>
        <Col sm={8}>
          <Form.Control
            type="password"
            name="newPassword"
            autoComplete="new-password"
            value={data.newPassword}
            onChange={onChange}
            required
          />
        </Col>
      </Form.Group>

      <Form.Group as={Row} controlId="new-password2-input">
        <Form.Label column sm={4}>
          Repeat password
        </Form.Label>
        <Col sm={8}>
          <Form.Control
            type="password"
            name="newPassword2"
            autoComplete="new-password"
            value={data.newPassword2}
            onChange={onChange}
            required
          />
        </Col>
      </Form.Group>

      <div className="text-end mt-3">
        <Button variant="primary" type="submit">
          Change password
        </Button>
      </div>
    </Form>
  );
};

PasswordChange.propTypes = {
  username: PropTypes.string.isRequired,
  changePassword: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  username: state.auth.user.username,
});

export default connect(mapStateToProps, { changePassword })(PasswordChange);
