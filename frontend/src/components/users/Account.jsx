import React from 'react';
import { Card } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { changeEmail, changeUsername } from '../../actions/auth';
import PasswordChange from './PasswordChange';
import SingleFieldForm from './SingleFieldForm';

const Account = ({ username, email, changeEmail, changeUsername }) => {
  return (
    <Card className="mx-auto" style={{ width: '30rem' }}>
      <Card.Body>
        <Card.Title as="h4">Account</Card.Title>
        <hr />
        <SingleFieldForm
          name="username"
          label="Username"
          initialValue={username}
          onSubmit={changeUsername}
        />
        <hr />
        <SingleFieldForm
          name="email"
          label="Email"
          initialValue={email}
          onSubmit={changeEmail}
        />
        <hr />
        <PasswordChange />
      </Card.Body>
    </Card>
  );
};

Account.propTypes = {
  username: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  changeEmail: PropTypes.func.isRequired,
  changeUsername: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  username: state.auth.user?.username,
  email: state.auth.user?.email,
});

export default connect(mapStateToProps, {
  changeEmail,
  changeUsername,
})(Account);
