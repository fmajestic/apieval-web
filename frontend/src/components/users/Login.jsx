import React, { useState } from 'react';
import { Button, Card, FloatingLabel, Form } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { login } from '../../actions/auth';

const Login = ({ login, isAuthenticated }) => {
  const initState = {
    username: '',
    password: '',
  };

  const [data, setData] = useState(initState);

  const onChange = (e) =>
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });

  const onSubmit = (e) => {
    e.preventDefault();
    login(data.username, data.password);
  };

  if (isAuthenticated) {
    return <Redirect push to="/" />;
  }

  return (
    <Card className="mx-auto" style={{ width: '25rem' }}>
      <Card.Body>
        <Card.Title as="h4">Log in</Card.Title>

        <hr />

        <Form onSubmit={onSubmit}>
          <FloatingLabel label="Username" controlId="username-field">
            <Form.Control
              type="text"
              name="username"
              autoComplete="username"
              placeholder=" "
              value={data.username}
              onChange={onChange}
              required
            />
          </FloatingLabel>

          <FloatingLabel label="Password" controlId="password-field">
            <Form.Control
              type="password"
              name="password"
              autoComplete="current-password"
              placeholder=" "
              value={data.password}
              onChange={onChange}
              required
            />
          </FloatingLabel>

          <div className="text-end mt-3">
            <Button variant="primary" type="submit">
              Log in
            </Button>
          </div>
        </Form>
      </Card.Body>

      <Card.Footer className="text-center">
        Don&#39;t have an account? <Link to="/register">Register</Link>
      </Card.Footer>
    </Card>
  );
};

Login.propTypes = {
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { login })(Login);
