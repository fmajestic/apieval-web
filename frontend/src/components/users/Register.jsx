import React, { useState } from 'react';
import { Button, Card, FloatingLabel, Form } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { register } from '../../actions/auth';
import { createErrors } from '../../actions/messages';

const Register = ({ register, createErrors, isAuthenticated }) => {
  const initState = {
    username: '',
    email: '',
    password: '',
    password2: '',
  };

  const [data, setData] = useState(initState);

  const onChange = (e) => {
    const { name, value } = e.target;
    setData({
      ...data,
      [name]: value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const { password, password2 } = data;

    if (password !== password2) {
      createErrors({ passwordMismatch: 'Passwords do not match' });
      return;
    }

    register(data);
  };

  // TODO: move this to Page, make an opposite of PrivateRoute, maybe GuestRoute
  if (isAuthenticated) {
    return <Redirect to="/" />;
  }

  // TODO: https://react-bootstrap.netlify.app/components/forms/#forms-validation
  return (
    <Card className="mx-auto" style={{ width: '30rem' }}>
      <Card.Body>
        <Card.Title as="h4">Register</Card.Title>

        <hr />

        <Form onSubmit={onSubmit}>
          <FloatingLabel label="Username" controlId="username-field">
            <Form.Control
              type="text"
              name="username"
              autoComplete="off"
              value={data.username}
              onChange={onChange}
              required
              placeholder=" "
            />
          </FloatingLabel>

          <FloatingLabel label="Email address" controlId="email-field">
            <Form.Control
              type="email"
              name="email"
              autoComplete="email"
              value={data.email}
              onChange={onChange}
              required
              placeholder=" "
            />
          </FloatingLabel>

          <FloatingLabel label="Password" controlId="password-field">
            <Form.Control
              type="password"
              name="password"
              autoComplete="new-password"
              value={data.password}
              onChange={onChange}
              required
              placeholder=" "
            />
            <Form.Text>At least 8 characters</Form.Text>
          </FloatingLabel>

          <FloatingLabel label="Repeat password" controlId="password2-field">
            <Form.Control
              type="password"
              name="password2"
              autoComplete="new-password"
              value={data.password2}
              onChange={onChange}
              required
              placeholder=" "
            />
          </FloatingLabel>

          <div className="text-end mt-3">
            <Button variant="primary" type="submit">
              Register
            </Button>
          </div>
        </Form>
      </Card.Body>
      <Card.Footer className="text-center">
        Already a user? <Link to="/login">Log in</Link>
      </Card.Footer>
    </Card>
  );
};

Register.propTypes = {
  isAuthenticated: PropTypes.bool,
  register: PropTypes.func.isRequired,
  createErrors: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { register, createErrors })(Register);
