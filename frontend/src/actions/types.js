// Tests
export const CREATE_TEST = 'CREATE_TEST';
export const DELETE_TEST = 'DELETE_TEST';
export const LOAD_TESTS = 'LOAD_TESTS';
export const LOAD_TEST_DETAILS = 'LOAD_TEST_DETAILS';
export const RUN_TEST = 'RUN_TEST';

// Alerts
export const CREATE_ERRORS = 'CREATE_ERRORS';
export const CREATE_MESSAGE = 'CREATE_MESSAGE';

// Auth
export const USER_LOADING = 'USER_LOADING';
export const USER_LOADED = 'USER_LOADED';
export const AUTH_ERROR = 'AUTH_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAIL = 'REGISTER_FAIL';
export const FIELD_CHANGED = 'FIELD_CHANGED';
export const TOKEN_CHANGED = 'TOKEN_CHANGED';
