import axios from '../util/axios';
import { createErrors, createMessage } from './messages';
import {
  AUTH_ERROR,
  FIELD_CHANGED,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  REGISTER_FAIL,
  REGISTER_SUCCESS,
  TOKEN_CHANGED,
  USER_LOADED,
  USER_LOADING,
} from './types';

export const register = (data) => (dispatch) => {
  return axios.post('/api/auth/register', data).then(
    (res) => dispatch({ type: REGISTER_SUCCESS, payload: res.data }),
    (err) => {
      dispatch({ type: REGISTER_FAIL });
      dispatch(createErrors(err.response.data, err.response.status));
    }
  );
};

export const loadUser = () => (dispatch) => {
  dispatch({ type: USER_LOADING });
  return axios.get('/api/auth/user').then(
    (res) => dispatch({ type: USER_LOADED, payload: res.data }),
    (err) => {
      dispatch({ type: AUTH_ERROR });
      dispatch(createErrors(err.response.data, err.response.status));
    }
  );
};

export const login = (username, password) => (dispatch) => {
  return axios.post('/api/auth/login', { username, password }).then(
    (res) => dispatch({ type: LOGIN_SUCCESS, payload: res.data }),
    (err) => {
      dispatch({ type: LOGIN_FAIL });
      dispatch(createErrors(err.response.data, err.response.status));
    }
  );
};

export const logout = () => (dispatch) => {
  axios.post('/api/auth/logout').then(
    () => dispatch({ type: LOGOUT_SUCCESS }),
    () => dispatch({ type: AUTH_ERROR })
  );
};

// TODO: this is terrible copy-paste, replace with one of these:
//       https://github.com/lelandrichardson/redux-pack
//       https://github.com/svrcekmichal/redux-axios-middleware
export const changeUsername = (username) => (dispatch) => {
  return axios.post('/api/auth/change/username', { username }).then(
    ({ data }) => {
      dispatch({
        type: FIELD_CHANGED,
        payload: { field: 'username', value: username },
      });
      dispatch(createMessage(data));
    },
    (err) => dispatch(createErrors(err.response.data, err.response.status))
  );
};

export const changeEmail = (email) => (dispatch) => {
  return axios.post('/api/auth/change/email', { email }).then(
    ({ data }) => {
      dispatch({
        type: FIELD_CHANGED,
        payload: { field: 'email', value: email },
      });
      dispatch(createMessage(data));
    },
    (err) => dispatch(createErrors(err.response.data, err.response.status))
  );
};

export const changePassword = (currentPassword, newPassword) => (dispatch) => {
  return axios
    .post('/api/auth/change/password', { currentPassword, newPassword })
    .then(
      ({ data }) => {
        dispatch(createMessage(data.message));
        dispatch({ type: TOKEN_CHANGED, payload: data.token });
      },
      (err) => {
        dispatch(createErrors(err.response.data, err.response.status));
      }
    );
};
