import axios from '../util/axios';
import { createErrors, createMessage } from './messages';
import {
  CREATE_TEST,
  DELETE_TEST,
  LOAD_TEST_DETAILS,
  LOAD_TESTS,
  RUN_TEST,
} from './types';

export const getTests = () => (dispatch) => {
  return axios.get('/api/tests').then(
    (res) => {
      dispatch({
        type: LOAD_TESTS,
        payload: res.data,
      });
    },
    (err) => dispatch(createErrors(err.response.data, err.response.status))
  );
};

export const deleteTest = (id) => (dispatch) => {
  return axios.delete(`/api/tests/${id}`).then(
    () => {
      dispatch(createMessage({ deleteTest: 'Test deleted' }));
      dispatch({
        type: DELETE_TEST,
        payload: id,
      });
    },
    (err) => dispatch(createErrors(err.response.data, err.response.status))
  );
};

export const createTest = (title, content, url, spec) => (dispatch) => {
  const specType = spec.name.split('.').pop().toUpperCase();

  return spec.text().then((specText) =>
    axios
      .post(`/api/tests`, { title, content, url, spec: specText, specType })
      .then(
        (res) => {
          dispatch(createMessage({ createTest: 'Test created' }));
          dispatch({
            type: CREATE_TEST,
            payload: res.data,
          });
        },
        (err) => {
          dispatch(createErrors(err.response.data, err.response.status));
        }
      )
  );
};

export const updateTest = (id, title, content, url, spec) => (dispatch) => {
  const specType = spec.name.split('.').pop().toUpperCase();

  return spec.text().then((specText) =>
    axios
      .put(`/api/tests/${id}`, {
        title,
        content,
        url,
        spec: specText,
        specType,
      })
      .then(
        (res) => {
          dispatch(createMessage({ createTest: 'Test created' }));
          dispatch({
            type: CREATE_TEST,
            payload: res.data,
          });
          return res.data;
        },
        (err) => {
          dispatch(createErrors(err.response.data, err.response.status));
        }
      )
  );
};

export const loadTestDetails = (id) => (dispatch) => {
  return axios.get(`/api/tests/${id}`).then(
    (res) => {
      dispatch({ type: LOAD_TEST_DETAILS, payload: res.data });
    },
    (err) => {
      dispatch(createErrors(err.response.data, err.response.status));
    }
  );
};

export const runTest = (id) => (dispatch) => {
  return axios.post(`/api/tests/${id}/run`).then(
    (res) => {
      // TODO: Make the test reducer:
      //       {
      //         tests, // simple
      //         details, // maybe even remove results, keep only array of ids
      //         results: { [allIds], {byId}}
      //       }
      //       Then in the details reducer, add it to list of ids
      //       And in results, actually add it. I may be overthinking this.
      dispatch({ type: RUN_TEST, payload: { testId: id, data: res.data } });
    },
    (err) => {
      dispatch(createErrors(err.response.data, err.response.status));
    }
  );
};

export const createFromFile = (file) => (dispatch) => {
  const data = new FormData();
  data.append('spec', file);

  return axios.post('/api/tests/spec', data).then(
    (res) => {
      dispatch(createMessage({ createTest: 'Test created' }));
      return res.data.id;
    },
    (err) => {
      dispatch(createErrors(err.response.data, err.response.status));
    }
  );
};
