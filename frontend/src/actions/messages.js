import { CREATE_ERRORS, CREATE_MESSAGE } from './types';

export const createMessage = (msg) => {
  return {
    type: CREATE_MESSAGE,
    payload: msg,
  };
};

export const createErrors = (msg, status) => {
  return {
    type: CREATE_ERRORS,
    payload: { msg, status },
  };
};
