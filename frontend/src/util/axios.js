import axios from 'axios';

const createInstance = () => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  let instance = axios.create(config);

  instance.interceptors.request.use((config) => {
    if (config.url === '/auth/login' || config.url === '/auth/register') {
      return config;
    }

    // TODO: subscribe to the store to watch for token changes? unlikely
    const token = localStorage.getItem('token');
    config.headers.Authorization = token ? `Token ${token}` : '';
    return config;
  });

  return instance;
};

export default createInstance();
