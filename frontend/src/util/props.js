const filterPresent = (propNames, props) =>
  propNames.filter((name) => Object.prototype.hasOwnProperty.call(props, name));

export const anyOf =
  (...propNames) =>
  (props) => {
    return filterPresent(propNames, props).length === 0
      ? new Error(`Must specify at least one of: [${propNames}]`)
      : null;
  };

export const oneOf =
  (...propNames) =>
  (props) => {
    const present = filterPresent(propNames, props);

    if (present.length === 0) {
      return new Error(`Must specify one of: [${propNames}]`);
    } else if (present.length > 1) {
      return new Error(
        `Must specify exactly one of: [${propNames}] (present: [${present}])`
      );
    } else {
      return null;
    }
  };
