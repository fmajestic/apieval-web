import {
  CREATE_TEST,
  DELETE_TEST,
  LOAD_TEST_DETAILS,
  LOAD_TESTS,
  LOGOUT_SUCCESS,
  RUN_TEST,
} from '../actions/types';

const initialState = {
  tests: [],
  testDetails: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOAD_TESTS:
      return {
        ...state,
        tests: action.payload,
      };
    case CREATE_TEST:
      return {
        ...state,
        tests: state.tests.concat([action.payload]),
      };
    case DELETE_TEST:
      return {
        ...state,
        tests: state.tests.filter((tests) => tests.id !== action.payload),
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        tests: [],
      };
    case LOAD_TEST_DETAILS:
      return {
        ...state,
        testDetails: {
          ...state.testDetails,
          [action.payload.id]: action.payload,
        },
      };
    // barf
    case RUN_TEST:
      return {
        ...state,
        testDetails: {
          ...state.testDetails,
          [action.payload.testId]: {
            ...state.testDetails[action.payload.testId],
            results: [action.payload.data].concat(
              state.testDetails[action.payload.testId].results
            ),
          },
        },
      };
    default:
      return state;
  }
};
