import {
  AUTH_ERROR,
  FIELD_CHANGED,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  REGISTER_FAIL,
  REGISTER_SUCCESS,
  TOKEN_CHANGED,
  USER_LOADED,
  USER_LOADING,
} from '../actions/types';

const initialState = {
  token: localStorage.getItem('token'),
  isAuthenticated: false,
  isLoading: true,
  user: null,
};

// TODO: split into multiple reducers, then export default combineReducers(...)
export default (state = initialState, action) => {
  switch (action.type) {
    case USER_LOADING:
      return {
        ...state,
        isAuthenticated: false,
        isLoading: true,
        user: null,
      };
    case USER_LOADED:
      return {
        ...state,
        isAuthenticated: true,
        isLoading: false,
        user: action.payload,
      };
    case AUTH_ERROR:
    case LOGIN_FAIL:
    case REGISTER_FAIL:
    case LOGOUT_SUCCESS:
      // Not terribly nice, but better than clearing everywhere we send these three actions
      localStorage.removeItem('token');
      return {
        isAuthenticated: false,
        isLoading: false,
        token: null,
        user: null,
      };
    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
      localStorage.setItem('token', action.payload.token);
      return {
        isAuthenticated: true,
        isLoading: false,
        token: action.payload.token,
        user: action.payload.user,
      };
    case FIELD_CHANGED:
      return {
        ...state,
        user: {
          ...state.user,
          [action.payload.field]: action.payload.value,
        },
      };
    case TOKEN_CHANGED:
      localStorage.setItem('token', action.payload);
      return {
        ...state,
        token: action.payload,
      };
    default:
      return state;
  }
};
