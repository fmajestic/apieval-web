import { combineReducers } from 'redux';

import auth from './auth';
import errors from './errors';
import messages from './messages';
import tests from './tests';

export default combineReducers({
  auth,
  tests,
  errors,
  messages,
});
