/* eslint-disable no-undef */
const path = require('path');

module.exports = (env, argv) => {
  return {
    // TODO: split configs https://webpack.js.org/guides/production/
    devtool: argv.mode === 'development' ? 'inline-source-map' : 'source-map',
    entry: path.resolve(__dirname, 'frontend/src/index.js'),
    output: {
      path: path.resolve(__dirname, 'frontend/static/frontend'),
      filename: 'main.js',
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: 'babel-loader',
        },
        {
          test: /\.(css|scss)$/,
          use: [
            { loader: 'style-loader' },
            {
              loader: 'css-loader',
              options: {
                // Fix for importing sass variables into js
                //  - https://github.com/facebook/create-react-app/issues/10047
                //  - https://github.com/webpack-contrib/css-loader/issues/1179
                modules: {
                  compileType: 'icss',
                },
              },
            },
            { loader: 'sass-loader' },
          ],
        },
      ],
    },
    resolve: {
      extensions: ['.js', '.jsx'],
    },
    // TODO: vendor/chunk splitting
    // TODO 2: tree shaking (check if sideEffects: [] does anything for prod bundle size)
  };
};
