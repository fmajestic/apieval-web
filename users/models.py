from django.contrib.auth.models import AbstractUser
from django.db import models

from utils.hashing import h_encode


class User(AbstractUser):
    email = models.EmailField(null=False, blank=False)  # Make email required

    class Meta:
        db_table = 'app_user'

    def get_hashid(self):
        return h_encode(self.pk)
