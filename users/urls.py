# noinspection PyPackageRequirements
import knox.views as knox_views
from django.urls import path

from .api import RegistrationAPI, LoginAPI, UserAPI, ChangeUsernameAPI, ChangeEmailAPI, ChangePasswordAPI

urlpatterns = [
    path('api/auth/register', RegistrationAPI.as_view()),
    path('api/auth/login', LoginAPI.as_view()),
    path('api/auth/user', UserAPI.as_view()),
    path('api/auth/logout', knox_views.LogoutView.as_view()),
    path('api/auth/change/email', ChangeEmailAPI.as_view()),
    path('api/auth/change/username', ChangeUsernameAPI.as_view()),
    path('api/auth/change/password', ChangePasswordAPI.as_view()),
]
