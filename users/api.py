from typing import cast

from django.contrib.auth.signals import user_logged_out, user_logged_in
# noinspection PyPackageRequirements
from knox.models import AuthToken, User
from rest_framework import generics, permissions, status
from rest_framework.request import Request
from rest_framework.response import Response

from .serializers import \
    UserSerializer, RegisterSerializer, LoginSerializer, \
    ChangeUsernameSerializer, ChangeEmailSerializer, ChangePasswordSerializer


def message(text: str, status_code: int = status.HTTP_200_OK):
    return Response({'message': text}, status_code)


def error(text: str, code: str = 'err', status_code: int = status.HTTP_400_BAD_REQUEST):
    return Response({'msg': {code: text}}, status_code)


class RegistrationAPI(generics.GenericAPIView):
    permission_classes = [permissions.AllowAny]

    serializer_class = RegisterSerializer

    def post(self, request: Request, *args, **kwargs):
        if request.user.is_authenticated:
            return error('Cannot register a new user while logged in!', status_code=status.HTTP_403_FORBIDDEN)

        serializer: RegisterSerializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        instance, token = AuthToken.objects.create(user)
        return Response({
            'user': UserSerializer(user, context=self.get_serializer_context()).data,
            'token': token
        })


class LoginAPI(generics.GenericAPIView):
    permission_classes = [permissions.AllowAny]

    serializer_class = LoginSerializer

    def post(self, request: Request, *args, **kwargs):
        serializer: LoginSerializer = self.get_serializer(data=request.data)
        # TODO: try-except for user_login_failed
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        instance, token = AuthToken.objects.create(user)
        user_logged_in.send(sender=user.__class__, request=request, user=user)
        return Response({
            'user': UserSerializer(user, context=self.get_serializer_context()).data,
            'token': token
        })


class ChangeUsernameAPI(generics.GenericAPIView):
    serializer_class = ChangeUsernameSerializer

    def post(self, request: Request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request.user.username = serializer.validated_data
        request.user.save()
        return message('Username changed successfully')


class ChangeEmailAPI(generics.GenericAPIView):
    serializer_class = ChangeEmailSerializer

    def post(self, request: Request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request.user.email = serializer.validated_data
        request.user.save()
        return message('Email changed successfully')


class ChangePasswordAPI(generics.GenericAPIView):
    serializer_class = ChangePasswordSerializer

    def post(self, request: Request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        new_password = serializer.validated_data
        user = cast(User, request.user)

        user.set_password(new_password)
        user.save()

        # Log out all sessions
        user.auth_token_set.all().delete()
        user_logged_out.send(sender=user.__class__, request=request, user=user)

        # Generate new token for current session
        _, token = AuthToken.objects.create(user)
        user_logged_in.send(sender=user.__class__, request=request, user=user)

        return Response({
            'message': 'Password changes successfully',
            'token': token
        }, status=status.HTTP_200_OK)


class UserAPI(generics.RetrieveAPIView):
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user
