from django.contrib.auth import authenticate
from rest_framework import serializers

from users.models import User


class UserSerializer(serializers.ModelSerializer):
    id = serializers.CharField(source='get_hashid', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'email')


class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user and user.is_active:
            return user
        else:
            raise serializers.ValidationError('Incorrect credentials')


# TODO:
#   - validate new username (length, chars, etc.)
#   - maybe use a ModelSerializer, see if it can pick up on model restrictions?
class ChangeUsernameSerializer(serializers.Serializer):
    username = serializers.CharField()

    def validate(self, data):
        username = data['username']
        count = User.objects.filter(username=username).count()

        if count == 0:
            return username
        else:
            raise serializers.ValidationError('Username already taken')


class ChangeEmailSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate(self, data):
        email = data['email']
        count = User.objects.filter(email=email).count()

        if count == 0:
            return email
        else:
            raise serializers.ValidationError('Email already taken')


class ChangePasswordSerializer(serializers.Serializer):
    current_password = serializers.CharField()
    new_password = serializers.CharField()

    def validate(self, data):
        user = self.context.get('request').user
        if user.check_password(data['current_password']):
            return data['new_password']
        else:
            raise serializers.ValidationError('Old password does not match')
